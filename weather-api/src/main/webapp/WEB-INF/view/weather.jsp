<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Weather Application</title>
</head>
<body>
	<h2>Weather App</h2>
	<select id="city-selector">
		<option value="2643743">London</option>
		<option value="3067696">Prague</option>
		<option value="5391959">San Francisco</option>
	</select>
	<button id="get-weather-btn">Get Weather</button>
	
	<br><br>
	
	<div id="current-weather">
		<h3></h3>
		<h4></h4>
	</div>
	
	<br><br>
	<div id="recents">
		<h4>Recent Searches</h4>
		<table id="logs-table" border="2px">
		</table>	
	</div>
	
<script src="/webjars/jquery/2.1.4/jquery.min.js"></script>
<script>
$(function(){
	
	$('div').hide();
	getRecents();
	
	$('#get-weather-btn').on('click', function() {
		$.ajax({
			url: '/getWeatherInfo/' +$('#city-selector').val(),
			success: function(weather) {
				console.log(weather);
				$('#current-weather').show();
				
				$('#current-weather h3').html("Today's Weather Condition in " +weather.name);
				$('#current-weather h4').html("Condition: " +weather.weather[0].description +"<br>Temperature: " +weather.main.temp +"F");
				
				getRecents();
			}
		})
	});
	
	function getRecents() {
		$.ajax({
			url: '/getRecents',
			success: function(logs) {
				console.log(logs);
				var inHTML = '';
				if(logs.length > 0) {	
					$('#recents').show();
					
					$.each(logs, function(i, log) {
						inHTML += "<tr><td>" +(i+1) +"</td><td>" +log.location +"</td><td>" +log.actualWeather +"</td><td>" +log.temperature +"</td></tr>";
					});
					
					$('#logs-table').html(inHTML);
				} 
			}
		})
	}
});
</script>
</body>
</html>