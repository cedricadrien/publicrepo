package com.consumer.api.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;

import com.consumer.api.model.WeatherInfo;
import com.consumer.api.model.WeatherLog;
import com.consumer.api.service.WeatherAppService;

@Controller
public class WeatherAppController {

	@Autowired
	private WeatherAppService weatherAppService;
	
	@GetMapping("/")
	public String index() {
		return "weather";
	}
	
	@GetMapping("/getWeatherInfo/{locationId}")
	@ResponseBody
	public WeatherInfo getWeatherInfo(@PathVariable Long locationId) {
		return weatherAppService.getWeatherInfo(locationId);
	}
	
	@GetMapping("/getRecents")
	@ResponseBody
	public List<WeatherLog> getRecents() {
		return weatherAppService.getRecents();
	}
}