package com.consumer.api.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.consumer.api.client.WeatherClient;
import com.consumer.api.dao.WeatherInfoDao;
import com.consumer.api.model.WeatherInfo;
import com.consumer.api.model.WeatherLog;

@Service
public class WeatherAppService {

	@Autowired
	WeatherClient client;
	
	@Autowired
	WeatherInfoDao weatherInfoDao;
	
	public WeatherInfo getWeatherInfo(Long locationId) {
		WeatherInfo weatherInfo = client.getWeatherInfo(locationId);
		if(weatherInfo != null) {
			WeatherLog log = new WeatherLog();
			log.setLocation(weatherInfo.getLocation());
			log.setActualWeather(weatherInfo.getWeather().get(0).getActualWeather());
			log.setTemperature(weatherInfo.getTemp().getCurrentTemp().toString());
			System.out.println("WEATHER LOG" +log);
			weatherInfoDao.save(log);
		}
		
		return weatherInfo; 
	}
	
	public List<WeatherLog> getRecents() {
		return weatherInfoDao.findFirst5ByOrderByIdDesc();
	}
}
