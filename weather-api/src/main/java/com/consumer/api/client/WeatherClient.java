package com.consumer.api.client;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import com.consumer.api.model.WeatherInfo;

@Component
public class WeatherClient {

	@Autowired
	RestTemplate restTemplate;
	private final String url;
	
	public WeatherClient(@Value("${weather.service.url}") String url) {
		this.url = url;
	}
	
	public WeatherInfo getWeatherInfo(Long locationId) {
		return restTemplate.getForObject(url, WeatherInfo.class, locationId);
	}
}
