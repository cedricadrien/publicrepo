package com.consumer.api.model;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;

@Entity
public class WeatherLog implements Serializable {

	private static final long serialVersionUID = 2684726411791627545L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int id;
	private String responseId;
	private String location;
	private String actualWeather;
	private String temperature;
	@CreationTimestamp
	private Timestamp dtimeInserted;
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getResponseId() {
		return responseId;
	}
	public void setResponseId(String responseId) {
		this.responseId = responseId;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public String getActualWeather() {
		return actualWeather;
	}
	public void setActualWeather(String actualWeather) {
		this.actualWeather = actualWeather;
	}
	public String getTemperature() {
		return temperature;
	}
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	public Timestamp getDtimeInserted() {
		return dtimeInserted;
	}
	public void setDtimeInserted(Timestamp dtimeInserted) {
		this.dtimeInserted = dtimeInserted;
	}
	@Override
	public String toString() {
		return "WeatherLog [id=" + id + ", responseId=" + responseId + ", location=" + location + ", actualWeather="
				+ actualWeather + ", temperature=" + temperature + ", dtimeInserted=" + dtimeInserted + "]";
	}
	
	
}
