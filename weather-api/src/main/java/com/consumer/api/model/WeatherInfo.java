package com.consumer.api.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown=true)
public class WeatherInfo {

	@JsonProperty("name")
	private String location;
	@JsonProperty("weather")
	private List<Weather> weather;
	@JsonProperty("main")
	private Temperature temp;
	
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public List<Weather> getWeather() {
		return weather;
	}
	public void setWeather(List<Weather> weather) {
		this.weather = weather;
	}
	public Temperature getTemp() {
		return temp;
	}
	public void setTemp(Temperature temp) {
		this.temp = temp;
	}
	@Override
	public String toString() {
		return "WeatherInfo [location=" + location + ", weather=" + weather + ", temp=" + temp + "]";
	}
	
	
}
